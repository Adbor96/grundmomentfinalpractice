﻿using UnityEngine;

public class Pickup : InteracteableClass
{
    public override void Interact()
    {
        base.Interact();

        PickupItem();
    }
    void PickupItem()
    {
        Debug.Log("Picking up item");
        //lägg till i inventory
        Destroy(gameObject);
    }
}
