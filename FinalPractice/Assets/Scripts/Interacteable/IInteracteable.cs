﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteracteable 
{
    void Interact();
}
public interface ICanPickUp
{
    event EventHandler PickupEvent;
}
/*public class Pickup : ICanPickUp
{
    public event EventHandler PickupEvent;
    void PickupFunction()
    {
        OnPickup();
    }
    protected virtual void OnPickup()
    {
        PickupEvent?.Invoke(this, EventArgs.Empty);
    }
}*/
