﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPooler : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        public string tag; //poolens tag
        public GameObject prefab; //prefaben av det som ska poolas
        public int size; //storleken av poolen aka hur många objekt det ska finnas i poolen
    }

    public static AttackPooler Instance;

    #region Singleton
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public List<Pool> poolsList; //lista över pools
    public Dictionary<string, Queue<GameObject>> poolDictionary; //håller koll på taggen av det som ska poolas samt vilket gameobject

    // Start is called before the first frame update
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in poolsList)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>(); //kö av kulans gameobject

            for (int i = 0; i < pool.size; i++) //fyll ut poolen genom att instantiate så många objekt som specifierat i "size"
            {
                GameObject obj = Instantiate(pool.prefab); //instantiate:a kulan
                obj.SetActive(false); //set attacken som inaktiv
                objectPool.Enqueue(obj); //sätt attacken i kön
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnfromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag)) //om dictionary får en tag som den inte har en pool för
        {
            Debug.LogWarning("Pool med tag " + tag + " finns inte.");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true); //sätt attacken som aktiv
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        IPooledAttacks pooledObjects = objectToSpawn.GetComponent<IPooledAttacks>();

        if (pooledObjects != null)
        {
            pooledObjects.OnAttackSpawn();
        }

        poolDictionary[tag].Enqueue(objectToSpawn);//sätter tillbaka attacken i kön

        return objectToSpawn;
    }
}

