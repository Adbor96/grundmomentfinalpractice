﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallAttack : MonoBehaviour
{
    AttackPooler attackPooler;

    /*public Rigidbody fireballPrefab;
    public Transform spawnPoint;
    public float force;*/
    // Start is called before the first frame update
    void Start()
    {
        attackPooler = AttackPooler.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            /*Rigidbody FireballProjectile;
            FireballProjectile = Instantiate(fireballPrefab, spawnPoint.position, spawnPoint.rotation) as Rigidbody;
            FireballProjectile.AddForce(spawnPoint.forward * force);*/
            attackPooler.SpawnfromPool("Fireball", transform.position, transform.rotation);
        }
    }
}
