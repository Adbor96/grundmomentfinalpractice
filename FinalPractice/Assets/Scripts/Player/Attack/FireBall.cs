﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour, IPooledAttacks
{
    public float upforce = 1;
    public float sideforce = .1f;
    public float force;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        IDamageable damageable = other.GetComponent<IDamageable>();
        if (damageable != null)
        {
            //hit a damageable object
            damageable.Damage(1);
            gameObject.SetActive(false);
        }
    }

    public void OnAttackSpawn()
    {
        /*float xForce = UnityEngine.Random.Range(-sideforce, sideforce);
        float yForce = UnityEngine.Random.Range(upforce / 2, upforce);
        float zForce = UnityEngine.Random.Range(-sideforce, sideforce);

        Vector3 force = new Vector3(xForce, -yForce, zForce);*/

        GetComponent<Rigidbody>().AddForce(transform.forward * force);
    }
}
