﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController controller;

    public float speed = 12;

    public Ray playerRay;
    public RaycastHit playerRayHit = new RaycastHit();

    public InteracteableClass focus;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 5, Color.green);

        //ray
        /*if (Input.GetKeyDown(KeyCode.E))
        {
            playerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(playerRay, out playerRayHit, 5f)) //om spelaren träffar något med sin interact-ray
            {
                Debug.Log("Interact");
                IInteracteable hitObject = playerRayHit.collider.GetComponent<IInteracteable>(); //spelarens ray hittar objekt som den kan interagera med
                if (hitObject != null)
                {
                    hitObject.Interact(); //spelaren interagerar med objektet den klickade på
                }
                return;
            }
        }*/
        //ray
        if (Input.GetKeyDown(KeyCode.E))
        {
            playerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(playerRay, out playerRayHit, 3f)) //om spelaren träffar något med sin interact-ray
            {
                InteracteableClass interacteable = playerRayHit.collider.GetComponent<InteracteableClass>(); //spelarens ray hittar objekt som den kan interagera med
                if (interacteable != null)
                {
                    SetFocus(interacteable);
                }
                //return;
            }
            else
            {
                RemoveFocus();
            }
        }
    }
    void SetFocus(InteracteableClass newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
                focus.OnDefocus();

            focus = newFocus;
        }
        newFocus.OnFocused(transform);
    }
    void RemoveFocus()
    {
        if (focus != null)
            focus.OnDefocus();

        focus = null;
    }
}
